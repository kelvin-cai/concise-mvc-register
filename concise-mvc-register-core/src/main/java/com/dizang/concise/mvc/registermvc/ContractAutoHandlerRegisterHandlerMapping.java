package com.dizang.concise.mvc.registermvc;

import org.springframework.http.MediaType;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo.BuilderConfiguration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.dizang.concise.mvc.util.ClassUtil;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Set;

/**
 * 这个是第三步 进行绑定url
 * 
 * @author kelvin
 *
 */
@Slf4j
public class ContractAutoHandlerRegisterHandlerMapping extends RequestMappingHandlerMapping {

    Set<String> basePackages;

    private RequestMappingInfo.BuilderConfiguration mappingInfoBuilderConfig;
    private boolean isGetSupperClassConfig = false;
    Boolean enable = false; 
    public ContractAutoHandlerRegisterHandlerMapping(Set<String> basePackages) {
        super();
        this.basePackages = basePackages;
        // 这里为了能够比SimpleMappingHandlerMapping早点使用，不然会匹配/** 的url
        super.setOrder(super.getOrder() - 1);
    	String property = System.getProperty("concise-mvc.enable","false");
    	enable = new Boolean(property);
    }

    /**
     * 判断是否符合触发自定义注解的实现类方法
     */
    @Override
    protected boolean isHandler(Class<?> beanType) {
        // 注解了 @Contract 的接口, 并且是这个接口的实现类
    	if (enable==false) {
			return false;
		}
        // 传进来的可能是接口，比如 FactoryBean 的逻辑
        if (beanType.isInterface())
            return false;

        // 是否是Contract的代理类，如果是则不支持
        if (ClassUtil.isContractTargetClass(beanType))
            return false;

        // 是否在包范围内，如果不在则不支持
        if (!isPackageInScope(beanType))
            return false;

        // 是否有标注了 @Contract 的接口
        Class<?> contractMarkClass = ClassUtil.getContractMarkClass(beanType);
        return contractMarkClass != null;
    }

    /**
     * 扫到实现类的方法时，与这里返回的url进行板顶
     */
    @Override
    protected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {
        Class<?> contractMarkClass = ClassUtil.getContractMarkClass(handlerType);
        try {
            // 查找到原始接口的方法，获取其注解解析为 requestMappingInfo
            Method originalMethod = contractMarkClass.getMethod(method.getName(), method.getParameterTypes());
            RequestMappingInfo info = buildRequestMappingByMethod(originalMethod);

            if (info != null) {
                RequestMappingInfo typeInfo = buildRequestMappingByClass(contractMarkClass);
                if (typeInfo != null)
                    info = typeInfo.combine(info);
            }
            return info;
        } catch (NoSuchMethodException ex) {
            return null;
        }
    }

    private RequestMappingInfo buildRequestMappingByClass(Class<?> contractMarkClass) {

        String simpleName = contractMarkClass.getSimpleName();
        String[] paths = new String[] { simpleName };
        RequestMappingInfo.Builder builder = RequestMappingInfo.paths(resolveEmbeddedValuesInPatterns(paths));

        // 通过反射获得 config
        if (!isGetSupperClassConfig) {
            BuilderConfiguration config = getConfig();
            this.mappingInfoBuilderConfig = config;
        }

        if (this.mappingInfoBuilderConfig != null)
            return builder.options(this.mappingInfoBuilderConfig).build();
        else
            return builder.build();
    }

    private RequestMappingInfo buildRequestMappingByMethod(Method originalMethod) {
        String name = originalMethod.getName();
        String[] paths = new String[] { name };
        // 用名字作为url
        // post形式
        // json请求
        RequestMappingInfo.Builder builder = RequestMappingInfo.paths(resolveEmbeddedValuesInPatterns(paths))
                .consumes(MediaType.APPLICATION_JSON_VALUE)
//                .params(requestMapping.params())
//                .mappingName(name)
//                .headers("Content-Type = application/json")
//                .produces(MediaType.APPLICATION_JSON_VALUE)
                .methods(RequestMethod.POST);
        return builder.options(this.getConfig()).build();
    }

    RequestMappingInfo.BuilderConfiguration getConfig() {
        Field field = null;
        RequestMappingInfo.BuilderConfiguration configChild = null;
        try {
            field = RequestMappingHandlerMapping.class.getDeclaredField("config");
            field.setAccessible(true);
            configChild = (RequestMappingInfo.BuilderConfiguration) field.get(this);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            log.error(e.getMessage(),e);
        } catch (NoSuchFieldException | SecurityException e) {
            log.error(e.getMessage(),e);
        }
        return configChild;
    }

    /**
     * 判断指定类是否在包范围内
     * 
     * @param beanType 指定类
     * @return 如果在范围内返回 true，否则返回 false
     */
    private boolean isPackageInScope(Class<?> beanType) {
        // 是否在包路径内
        String packageName = ClassUtils.getPackageName(beanType);
        boolean isPackageScope = false;
        for (String basePackage : basePackages) {
            if (packageName.startsWith(basePackage)) {
                isPackageScope = true;
                break;
            }
        }
        return isPackageScope;
    }

}
