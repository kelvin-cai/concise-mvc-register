package com.dizang.concise.mvc.resolver;

import org.springframework.core.MethodParameter;

public class ContractRequestBodyArgumentResolver extends RequestBodyArgumentResolver implements DefaultMethodArgumentResolverSupportable {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return DefaultMethodArgumentResolverSupportable.super.supportsParameter(parameter, super::supportsParameter);
    }
    
}
