package com.dizang.concise.mvc.resolver;

import org.springframework.core.MethodParameter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

import com.dizang.concise.mvc.util.ClassUtil;

import java.util.List;

/**
 * @Contract注解类参数返回值处理器，统一按 @ResponseBody 方式处理
 */
public class ContractRequestResponseBodyMethodProcessor extends RequestResponseBodyMethodProcessor implements DefaultMethodArgumentResolverSupportable {

    public ContractRequestResponseBodyMethodProcessor(List<HttpMessageConverter<?>> converters) {
        super(converters);
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return DefaultMethodArgumentResolverSupportable.super.supportsParameter(parameter, super::supportsParameter);
    }

    /**
     * 匹配中@Contract注解的，就能判断为能使用这个@ResponseBody方式
     */
    @Override
    public boolean supportsReturnType(MethodParameter returnType) {
        Class<?> declaringClass = returnType.getMethod().getDeclaringClass();
        Class<?> feignClientMarkClass = ClassUtil.getContractMarkClass(declaringClass);
        return feignClientMarkClass != null;
    }
}
