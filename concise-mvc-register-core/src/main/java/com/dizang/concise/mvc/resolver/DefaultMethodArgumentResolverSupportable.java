package com.dizang.concise.mvc.resolver;

import org.springframework.core.MethodParameter;

import com.dizang.concise.mvc.util.ClassUtil;

public interface DefaultMethodArgumentResolverSupportable {

    /**
     * 这里只是为了能让实现类的方法，能够找到它对应的interface的方法后。执行对应的判断，如找@Pathvaluable。
     * @param parameter
     * @param able
     * @return
     */
    default boolean supportsParameter(MethodParameter parameter, Supportable able) {
        MethodParameter originalMethod = ClassUtil.getContractInterfaceMethodParameter(parameter);
        if (originalMethod == null)
            return false;
        return able.support(originalMethod);
    }

    @FunctionalInterface
    interface Supportable {
        boolean support(MethodParameter parameter) ;
    }

}
