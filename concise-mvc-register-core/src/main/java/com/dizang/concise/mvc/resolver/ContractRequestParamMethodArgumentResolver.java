package com.dizang.concise.mvc.resolver;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.method.annotation.RequestParamMethodArgumentResolver;

public class ContractRequestParamMethodArgumentResolver extends RequestParamMethodArgumentResolver implements DefaultMethodArgumentResolverSupportable {

    public ContractRequestParamMethodArgumentResolver(ConfigurableBeanFactory beanFactory, boolean useDefaultResolution) {
        super(beanFactory, useDefaultResolution);
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return DefaultMethodArgumentResolverSupportable.super.supportsParameter(parameter, super::supportsParameter);
    }
}
