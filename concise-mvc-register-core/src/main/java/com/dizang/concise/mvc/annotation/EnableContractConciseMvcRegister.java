package com.dizang.concise.mvc.annotation;

import org.springframework.context.annotation.Import;

import com.dizang.concise.mvc.registermvc.ContractAutoHandlerRegisterConfiguration;

import java.lang.annotation.*;

/**
 * 这个是第一步
 * springboot启动时需要添加这个注解，并添加 basePackages扫实现类
 * @author kelvin
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(ContractAutoHandlerRegisterConfiguration.class)
public @interface EnableContractConciseMvcRegister {

    /**
     * Contract 注解的请求包扫描路径
     * @return
     */
    String[] basePackages() default {};

}
