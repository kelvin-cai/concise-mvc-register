package com.dizang.concise.mvc.config;

import static springfox.documentation.schema.Collections.collectionElementType;
import static springfox.documentation.schema.Collections.isContainerType;

import java.util.Collection;
import java.util.Collections;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.classmate.ResolvedType;

import springfox.documentation.service.ResolvedMethodParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ParameterBuilderPlugin;
import springfox.documentation.spi.service.contexts.ParameterContext;
import springfox.documentation.spring.web.readers.parameter.ParameterTypeReader;

/**
 * @author caijunhao
 * @version 创建时间：2020年10月13日 上午11:28:03 类说明
 */
//@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CustomParameterTypeReader implements ParameterBuilderPlugin {

	public static String findParameterType(ParameterContext parameterContext) {
	    ResolvedMethodParameter resolvedMethodParameter = parameterContext.resolvedMethodParameter();
	    ResolvedType parameterType = resolvedMethodParameter.getParameterType();
	    parameterType = parameterContext.alternateFor(parameterType);
		return "body";
	}

	@Override
	public boolean supports(DocumentationType delimiter) {
		return true;
	}

	@Override
	public void apply(ParameterContext context) {
		String parameterType = findParameterType(context);
		context.parameterBuilder().parameterType(parameterType);
		Collection<MediaType> accepts = "body".equals(parameterType) ? Collections.singleton(MediaType.APPLICATION_JSON)
				: null;
		context.requestParameterBuilder().in(parameterType).accepts(accepts);

	}

}
