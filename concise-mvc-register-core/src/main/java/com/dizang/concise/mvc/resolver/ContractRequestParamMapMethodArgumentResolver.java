package com.dizang.concise.mvc.resolver;

import org.springframework.core.MethodParameter;
import org.springframework.web.method.annotation.RequestParamMapMethodArgumentResolver;

public class ContractRequestParamMapMethodArgumentResolver extends RequestParamMapMethodArgumentResolver implements DefaultMethodArgumentResolverSupportable {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return DefaultMethodArgumentResolverSupportable.super.supportsParameter(parameter, super::supportsParameter);
    }
}
