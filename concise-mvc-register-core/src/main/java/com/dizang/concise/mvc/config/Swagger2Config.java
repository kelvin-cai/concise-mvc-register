package com.dizang.concise.mvc.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.ParameterSpecificationProvider;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.builders.SimpleParameterSpecificationBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ParameterType;
import springfox.documentation.service.RequestParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/** 
* @author 作者 kelvin E-mail: kelvin439714688@163.com
* @version 创建时间：2020年10月12日 下午8:52:38 
* 类说明 
*/
//@Configuration
public class Swagger2Config implements WebMvcConfigurer {

    //配置content type
    private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES =
            new HashSet<String>(Arrays.asList("application/json")); 
    
    // 根据 docket 配置多个 分组        
    @Bean()
    public Docket baseDocket() {
        List<RequestParameter> parameters = new ArrayList<>();
        parameters.add(new RequestParameterBuilder()
                .name("content-type")
                .description("application/json")
                .in(ParameterType.HEADER)
                .required(false)
                .build());
        return new Docket(DocumentationType.OAS_30)
                .consumes(DEFAULT_PRODUCES_AND_CONSUMES)
                .produces(DEFAULT_PRODUCES_AND_CONSUMES)
                .globalRequestParameters(parameters)
                .useDefaultResponseMessages(false)
                .select()
//                .apis(RequestHandlerSelectors.withClassAnnotation(Controller.class))//根据注解Controller 找接口
                .paths(PathSelectors.any())//全路径的 接口
                .build();
    }
    


}
