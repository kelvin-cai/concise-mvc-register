package com.dizang.concise.mvc.resolver;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.method.annotation.ExpressionValueMethodArgumentResolver;

public class ContractExpressionValueMethodArgumentResolver extends ExpressionValueMethodArgumentResolver implements DefaultMethodArgumentResolverSupportable {

    public ContractExpressionValueMethodArgumentResolver(ConfigurableBeanFactory beanFactory) {
        super(beanFactory);
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return DefaultMethodArgumentResolverSupportable.super.supportsParameter(parameter, super::supportsParameter);
    }
}
