package com.dizang.concise.mvc.common.util;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * Created by liangqq on 2016/9/14.
 */
public abstract class ValidateUtils {

    private static Validator validator;

    static {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    public static <T> String validateBean(T bean) {
        Set<ConstraintViolation<T>> violations = validator.validate(bean);
        StringBuilder errMsg = new StringBuilder();
        for (ConstraintViolation<T> violation : violations) {
            errMsg.append(violation.getPropertyPath().toString() + " " + violation.getMessage() + ", ");//
        }
        return  errMsg.toString();
    }
}
