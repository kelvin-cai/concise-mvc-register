package com.dizang.concise.mvc.common.util;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ResponseEntity<T> implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 2946260266591132523L;

    private T body;

    private Integer code;
    
    private String message;
    
    public ResponseEntity(T body ,HttpStatus status){
        this.body =body;
        if (status!=null) {
            this.code = status.value();
        }
    }
    
    public static <T> ResponseEntity<T> ok(T body) {
        return new ResponseEntity<T>(body,HttpStatus.OK);
    }
    
    public static <T> ResponseEntity<T> fail(String body) {
        ResponseEntity<T> responseEntity = new ResponseEntity<T>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        responseEntity.setMessage(body);
        return responseEntity;
    }
    
    public static <T> ResponseEntity<T> illgalArgument(String message) {
        return new ResponseEntity<T>(null,HttpStatus.NOT_IMPLEMENTED.value(),message);
    }
}
