package com.dizang.concise.mvc;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.dizang.concise.mvc.annotation.EnableContractConciseMvcRegister;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * 
 * Title: SampleWebJspApplication 
 * Description: 
 * Company:
 * 
 * @author 蔡俊豪 cocaijunhao
 *
 */
@Configuration
@EnableAutoConfiguration 
@ComponentScan
@SpringBootApplication
//@EnableSwagger2
@EnableContractConciseMvcRegister(basePackages = "com.dizang.concise.mvc.controller.impl")
public class ConsicesMvcApplication {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(ConsicesMvcApplication.class, args);
	}

}