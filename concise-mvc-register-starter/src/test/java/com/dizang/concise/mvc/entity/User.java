package com.dizang.concise.mvc.entity;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6721959704443837031L;
	
	@NotNull
    private Integer age;

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
	
}
