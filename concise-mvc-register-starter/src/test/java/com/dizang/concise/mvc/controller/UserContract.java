package com.dizang.concise.mvc.controller;

import com.dizang.concise.mvc.common.annotation.Contract;
import com.dizang.concise.mvc.common.util.ResponseEntity;
import com.dizang.concise.mvc.entity.User;

@Contract
public interface UserContract {
    
//    User getUser(Integer id,Integer name,User user);
    
    ResponseEntity<User> getUserBody(User user);
}
