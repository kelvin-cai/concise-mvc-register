package com.dizang.concise.mvc.controller.impl;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.dizang.concise.mvc.common.annotation.Contract;
import com.dizang.concise.mvc.common.util.ResponseEntity;
import com.dizang.concise.mvc.controller.UserContract;
import com.dizang.concise.mvc.entity.User;


@Contract
@Component
public class UserContractImpl implements UserContract {
	
    /**
     * 如果有实体类，必须注解@Requestbody
     * 如果什么都不加，默认都是form形式提交基本类型
     */
//    @Override
//    public User getUser(@RequestParam Integer id, @RequestParam Integer name, @RequestBody User user) {
//        user.setAge(id);
//        return user;
//    }

    /**
     * 默认Requestbodybody提交
     */
    @Override
    public ResponseEntity<User> getUserBody( User user11) {
        return ResponseEntity.ok(user11);
    }

}
