package com.dizang.concise.mvc.config;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component(value = "conciseMvcRegisterConfig")
@ConfigurationProperties(prefix = "concise-mvc-register")
public class ConciseMvcRegisterConfig implements InitializingBean {

	private boolean enable;
	

	public boolean isEnable() {
		return enable;
	}


	public void setEnable(boolean enable) {
		this.enable = enable;
	}


	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("动态MVC生成器运行状态:{}", enable);
		System.setProperty("concise-mvc.enable", enable + "");

	}


}
