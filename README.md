# concise-mvc-register

#### 介绍
使用自定义注解，让其自动按照interface的类名和方法名来生成url，不再需要填写@RequestMapping(name)，也不再需要指定请求方式、accept-type等参数

#### 软件架构
软件架构说明
1. 基于springboot、springMVC组件制作。

2. 使用自定义注解，并在服务启动时注册MVC映射关系和MVC请求返回特殊处理。

3. 每个请求URL由使用@Contract的注解的interface的类名、方法名组成，默认@RequestBody请求和ResponseBody返回

4. 如果要使用多个参数时，请求接口的实现类的方法需要显示声明每个参数的解析方式如@PathVariable


#### 安装教程

1.  mvn 打包 deploy 成snapshot 上传到仓库，在一个springboot项目中下载

#### 使用说明

1. @Contract使用在有interface的实现类上，不用带任何springMvc注解，除非想指定解析方式
2. 在springboot main运行类上，添加注解@EnableContractConciseMvcRegister(basePackages = "这里填接口实现类的包路径，不用带/.*")
3. 运行springboot
4. 使用post请求，localhost：端口/类名/方法名，application/json形式请求接口
5. 使用时代码样式如test目录下的。但是本工程的test代码只是样例，你需要在一个新工程使用@EnableContractConciseMvcRegister。
6. @Contract注解放在实现类上，就可以生成url，不用放在interface上，因为一些服务rpc框架喜欢idl生成出interface
7. 实现类必须已进入spring的作为bean
8. 请求参数必须有getter setter

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
